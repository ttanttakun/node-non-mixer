# ttanttakun/node-non-mixer

[Non Mixer](http://non.tuxfamily.org/wiki/index.php?page=Non%20Mixer) OSC control module for Node.js.

As for now, there is only one interesting method available: `fade`.

## Install
`npm install git+https://gitlab.com/ttanttakun/node-non-mixer.git`

## Load

`var Mixer = require('node-non-mixer');`

## Init

`var mixer = new Mixer(params);`

|param|description|default|
|---|---|---|
|localAddress| Network device address for the Module's UDP client/server. use an  | 127.0.0.1 |
|localPort| module's UDP client/server port | 57121 |
|remoteAddress|Non Mixer host ip|127.0.0.1|
|**remotePort**|Non Mixer OSC port (set ite in Non Mixer with `--osc-port`, as in `non-mixer --osc-port 10000`)|---|
|**strips**|Object with Non Mixer *strip* names as keys. Used to initialize each *strip*'s dB level| ---|
|oscMsgDelay|Delay between OSC messages ( in *ms* )|10|


## Mixer methods

### .fade(args)
|param|description|default|
|---|---|---|
|**strip**|Non Mixer strip name to fade| --- |
|**dB**|dB (6.0 > dB < -70.0) to fade to| ---|
|time|fade time ( in *ms* )|2000|
|callback|callback function will be called when fade ends with *strip*'s current dB level as argument|---|

### .close()
Close UPD server/client

## Example

```
var Mixer = require('node-non-mixer');

var mixer = new Mixer({
  remotePort: 10000, // Non Mixer is running in localhost, on port 10000
  strips: {
    main: {
      currentdB: 4.0,
      setCurrentDB: true
    }
  }
});

mixer.fade({
  strip: "main",
  time: 10000,
  dB: -70.0,
  callback: function(dB){
    console.log("done: " + dB);
    mixer.close();
  }
});

```
