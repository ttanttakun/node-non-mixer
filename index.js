var osc = require("osc");

function __send(udpPort, strip, dB){
  udpPort.send({
      address: "/strip/" + strip + "/Gain/Gain%20(dB)/unscaled",
      args: dB
  });
}

function Mixer(args) {
  this.args = args;
  this.strips = args.strips;
  this.udpPort = new osc.UDPPort({
    localAddress: args.localAddress || "127.0.0.1",
    localPort: args.localPort || 57121,
    remoteAddress: args.remoteAddress || "127.0.0.1",
    remotePort: args.remotePort
  });

  this.udpPort.open();

  for (var strip in this.strips) {
    if (this.strips.hasOwnProperty(strip)) {
      if(this.strips[strip].setCurrentDB) {
        __send(this.udpPort, this.strips[strip].name, this.strips[strip].currentdB );
      }
    }
  }
}

Mixer.prototype.close = function(){
  this.udpPort.close();
};

Mixer.prototype.fade = function fade(args) {
  if(!args.strip) return console.error('Please specify `strip`');
  if(!args.dB) return console.error('Please specify `dB`');

  var self = this;
  var dB = args.dB;
  var strip = args.strip;
  var time = args.time ? args.time : 2000;

  if (dB > 6) {
    dB = 6.0;
  } else if (dB < -70) {
    dB = -70.0;
  } else if (dB === self.strips[strip].currentdB) {
    return;
  }

  var delta = dB - self.strips[strip].currentdB;
  var direction = delta > 0 ? true : false;
  var delay = self.args.oscMsgDelay || 10;
  var tick = (delta * delay) / time;

  function recurse(){
    var currentdB = self.strips[strip].currentdB;
    if( ( direction && currentdB < dB ) ||  !direction && currentdB > dB ) {
      self.strips[strip].currentdB = currentdB + tick;
      __send(self.udpPort, strip, currentdB);
      setTimeout(function(){
        recurse();
      }, delay);
    } else {
      if(args.callback) return args.callback(currentdB);
    }
  }
  recurse();
};

module.exports = Mixer;
