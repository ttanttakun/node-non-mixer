var Mixer = require('../index.js');

var mixer = new Mixer({
  localAddress: "0.0.0.0", // use `public` device address to control a remote Non Mixer instance 
  remoteAddress: "192.168.1.115",
  remotePort: 10000,
  strips: {
    main: {
      currentdB: 4.0,
      setCurrentDB: true
    }
  }
});

mixer.fade({
  strip: "main",
  time: 10000,
  dB: -70.0,
  callback: function(dB){
    console.log("done: " + dB);
    mixer.close();
  }
});
